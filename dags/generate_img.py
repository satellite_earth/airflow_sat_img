
"""Description: dag to generate images with sensor.
"""

import subprocess

from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators import CheckHritSensor


def generate_img_main(ds, **kwargs):
    """Description: generate img main.
    """
    # Get date
    next_execution_date = (kwargs["next_execution_date"] - timedelta(minutes=0)).strftime("%Y%m%d%H%M")
    execution_date = kwargs["execution_date"].strftime("%Y%m%d%H%M")
    # Run images packages without sensor
    # subprocess.call("airflow_generate_images -t='" + str(execution_date)+"'", shell=True)
    # logger.info("Slot: ", next_execution_date, execution_date, kwargs["next_execution_date"])


# dag # 8,23,38,53 * * * *',
dag = DAG('generate_img', description='Generate images',
          schedule_interval='0,15,30,45 * * * *',
          start_date=datetime(2017, 11, 3), catchup=False)

# Sensor
sensor_task = CheckHritSensor(
    task_id='Check_hrit_files',
    filepath="/home/user/data/",
    poke_interval=10,
    timeout=23*60,
    dag=dag)

# dummy operator
#dummy_operator = DummyOperator(task_id='dummy_task', retries=3, dag=dag)

# img operator
img_operator = PythonOperator(task_id='generate_img_task', provide_context=True,
                              python_callable=generate_img_main, dag=dag)

# dummy_operator >> 
sensor_task >> img_operator

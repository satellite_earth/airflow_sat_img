
"""Description: Check Hrit Sensor.
"""

import logging
import glob
from datetime import datetime
from airflow.models import BaseOperator
from airflow.plugins_manager import AirflowPlugin
from airflow.utils.decorators import apply_defaults
from airflow.operators.sensors import BaseSensorOperator

log = logging.getLogger(__name__)

class CheckHritSensor(BaseSensorOperator):

    @apply_defaults
    def __init__(self, filepath, *args, **kwargs):
        super(CheckHritSensor, self).__init__(*args, **kwargs)
        self.filepath = filepath


    def poke(self, context):
        # Get path and execution_date
        path = self.filepath
        date_file = context["execution_date"].strftime("%Y%m%d%H%M")

        # get number files for execution_date
        number_files = len(glob.glob(path + "*" + date_file + "*"))
        if number_files < 74:
            log.warning("Do not exist all HRIT files: " + str(number_files))
            return False
        else:
            log.info("Exist all HRIT files." + str(number_files))
            return True


class CheckHritPlugin(AirflowPlugin):
    name = "check_hrit_plugin"
    operators = [CheckHritSensor]
